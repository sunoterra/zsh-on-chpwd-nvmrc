####
#### file:        zsh-on-chpwd-nvmrc.zsh
#### description: enables a change-directory hook that looks for a local .nvmrc file.
####              if not file exists, nothing is done. if a file does exist, and the
####              specified version of node is installed on the system, then use that
####              node version; otherwise, retain current node version, or upon failure
####              of everything else, use the node version set to nvm's default.
#### references:  https://github.com/creationix/nvm#zsh
#### author:      https://github.com/creationix/nvm and Michael Stilson <michael.stilson@me.com>
#### version:     v1.0.0
####
autoload -U add-zsh-hook
zsh-on-chpwd-nvmrc() {
  local current default nvmrc nvmrc_version
  nvmrc=$(nvm_find_nvmrc)
  if [ -n "${nvmrc}" ] # file found
  then # get the version string from it
    nvmrc_version=$(grep -v "^#.*" "${nvmrc}") # allow lines that begin # to be comments
  fi
  if [ -n "${nvmrc_version}" ] # version string
  then # check to see if it's installed
    current=$(nvm version)
    potential=$(nvm version "${nvmrc_version}")
    if [[ "${potential}" == "N/A" ]] # not installed
    then # feedback and exit
      echo "${0}: version ${nvmrc_version} is currently not installed"
      echo "  (specified by ${nvmrc})"
      echo "* retaining version ${current}"
      return # nothing to do
    fi
    if [[ "${potential}" != "${current}" ]]
    then #load the nvmrc version
      nvm use "${nvmrc_version}"
    fi
    return # we're done
  fi
  current=$(nvm version)
  default=$(nvm version default)
  if [ "$(nvm version)" != "${default}" ]
  then # switch back from whatever dir with an .nvmrc we came from
    echo "${0}: reverting to ${default} version"
    nvm use "${default}"
  fi
}
add-zsh-hook chpwd zsh-on-chpwd-nvmrc
zsh-on-chpwd-nvmrc
# }}}
# vim:foldmethod=marker:foldlevel=0
